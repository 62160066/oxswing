  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.oxswing;

import java.io.Serializable;

/**
 *
 * @author W.Home
 */
public class Player implements Serializable{
     char name;
    int win;
     int lose;
     int draw;
    public Player (char c){
        this.name = c;
        win =0;
        lose=0;
        draw=0;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }
    public void win(){
        win++;
    }

   

    public int getLose() {
        return lose;
    }
    public void lose(){
        lose++;
    }
    

   

    public int getDraw() {
        return draw;
    }
    public void draw(){
        draw++;
    }

   
    
}
